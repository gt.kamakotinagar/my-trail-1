# docker

## php

Docker Image for PHP >= 7.4 based on the [official php repository](https://hub.docker.com/_/php/), but comes with additional libraries and tools.

Images:

- `registry.gitlab.com/webgears-gmbh/docker/php:7.4`
- `registry.gitlab.com/webgears-gmbh/docker/php:7.4-node12` - contains node 12
- `registry.gitlab.com/webgears-gmbh/docker/php:7.4-node12-fpm` - contain apache and fpm and node 12
- `registry.gitlab.com/webgears-gmbh/docker/php:7.4-node14` - contains node 14
- `registry.gitlab.com/webgears-gmbh/docker/php:7.4-node14-fpm` - contains apache and fpm and node 14
- `registry.gitlab.com/webgears-gmbh/docker/php:7.4-node16` - contains node 16
- `registry.gitlab.com/webgears-gmbh/docker/php:7.4-node16-fpm` - contains apache and fpm and node 16
- `registry.gitlab.com/webgears-gmbh/docker/php:7.4-python-node14` - contains python and node 14
- `registry.gitlab.com/webgears-gmbh/docker/php:7.4-python-node16` - contains python and node 16
- `registry.gitlab.com/webgears-gmbh/docker/php:7.4-eb` - contains elastic beanstalk cli
- `registry.gitlab.com/webgears-gmbh/docker/php:7.4-node14-dependency-scan` - contains dependency scan with node 14
- `registry.gitlab.com/webgears-gmbh/docker/php:7.4-node16-dependency-scan` - contains dependency scan with node 16
- `registry.gitlab.com/webgears-gmbh/docker/php:8.0`
- `registry.gitlab.com/webgears-gmbh/docker/php:8.0-node16` - contains node 16
- `registry.gitlab.com/webgears-gmbh/docker/php:8.0-node16-fpm` - contains apache and fpm and node 16
- `registry.gitlab.com/webgears-gmbh/docker/php:8.0-python` - contains python and node 16
- `registry.gitlab.com/webgears-gmbh/docker/php:8.0-eb` - contains elastic beanstalk cli
- `registry.gitlab.com/webgears-gmbh/docker/php:8.0-node16-dependency-scan` - contains dependency scan with node 16
- `registry.gitlab.com/webgears-gmbh/docker/php:8.1`
- `registry.gitlab.com/webgears-gmbh/docker/php:8.1-node16` - contains node 16
- `registry.gitlab.com/webgears-gmbh/docker/php:8.1-node16-fpm` - contains apache and fpm and node 16
- `registry.gitlab.com/webgears-gmbh/docker/php:8.1-python` - contains python and node 16
- `registry.gitlab.com/webgears-gmbh/docker/php:8.1-eb` - contains elastic beanstalk cli
- `registry.gitlab.com/webgears-gmbh/docker/php:8.1-node16-dependency-scan` - contains dependency scan with node 16

## cypress

Image: `registry.gitlab.com/webgears-gmbh/docker/cypress:latest`

Contains binaries and services required for running cypress tests

- pm2
- cypress
- typescript
- eslint
- eslint-config-molindo
- memcached 
