#!/usr/bin/env bash

apt-get --allow-releaseinfo-change update
apt-get update

apt-get install -y python3-pip
pip3 install pipenv

git clone https://${DEPENDENCY_SCAN_DEPLOY_TOKEN_USER}:${DEPENDENCY_SCAN_DEPLOY_TOKEN_VALUE}@gitlab.com/webgears-gmbh/dependency-scan.git
cp dependency-scan/main.py /script/
cp dependency-scan/sheets.py /script/
cp -r dependency-scan/parser/. /script/parser
cp dependency-scan/Pipfile /script/
cp dependency-scan/Pipfile.lock /script/

pipenv install --deploy
