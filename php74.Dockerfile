#################
#### PHP 7.4 ####
#################
FROM php:7.4-buster AS php74

RUN apt clean
RUN apt update
ADD ./scripts /scripts
ADD ./bin/slack /usr/local/bin
RUN /scripts/php_install.sh
RUN apt install jq rsync -yqq

#################
#### Node 14 ####
#################

FROM php74 AS php74-node14
RUN /scripts/node14_install.sh
# Install autoreconf to fix gulp-imagin issue
RUN apt install dh-autoreconf -yqq


#################
#### Node 16 ####
#################

FROM php74 AS php74-node16
RUN /scripts/node16_install.sh
# Install autoreconf to fix gulp-imagin issue
RUN apt install dh-autoreconf -yqq

################
#### Python  ###
################

FROM php74-node14 AS php74-python-node14
RUN apt-get install jq build-essential python-dev curl wget xz-utils zlib1g-dev libreadline-dev libxml2-dev llvm tk-dev make libxmlsec1-dev libbz2-dev libssl-dev libsqlite3-dev libffi-dev git liblzma-dev libncursesw5-dev postgresql -yqq
RUN apt-get update
RUN curl -L https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash
RUN echo "export PATH=\"/root/.pyenv/bin:\$PATH\"" >> ~/.bashrc
RUN . ~/.bashrc && pyenv install 3.7.2
ENV PATH="/root/.pyenv/versions/3.7.2/bin:$PATH"
RUN pip install pipenv
RUN apt-get clean

FROM php74-node16 AS php74-python-node16
RUN apt-get install jq build-essential python-dev curl wget xz-utils zlib1g-dev libreadline-dev libxml2-dev llvm tk-dev make libxmlsec1-dev libbz2-dev libssl-dev libsqlite3-dev libffi-dev git liblzma-dev libncursesw5-dev postgresql libpq-dev -yqq
RUN apt-get update
RUN curl -L https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash
RUN echo "export PATH=\"/root/.pyenv/bin:\$PATH\"" >> ~/.bashrc
RUN . ~/.bashrc && pyenv install 3.7.2
ENV PATH="/root/.pyenv/versions/3.7.2/bin:$PATH"
RUN pip install pipenv
RUN apt-get clean


#########################
#### Dependency Scan  ###
#########################

FROM php74-python-node14 AS php74-node14-dependency-scan
ARG DEPENDENCY_SCAN_DEPLOY_TOKEN_USER
ARG DEPENDENCY_SCAN_DEPLOY_TOKEN_VALUE
ENV LANG=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8
ENV LC_CTYPE=en_US.UTF-8
WORKDIR /script
RUN /scripts/dependency-scan.sh
RUN wget https://get.symfony.com/cli/installer -O - | bash && mv /root/.symfony5/bin/symfony /usr/local/bin/symfony
CMD [ "pipenv", "run", "python", "./dependency-scan/main.py" ]

FROM php74-python-node16 AS php74-node16-dependency-scan
ARG DEPENDENCY_SCAN_DEPLOY_TOKEN_USER
ARG DEPENDENCY_SCAN_DEPLOY_TOKEN_VALUE
ENV LANG=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8
ENV LC_CTYPE=en_US.UTF-8
WORKDIR /script
RUN /scripts/dependency-scan.sh
RUN wget https://get.symfony.com/cli/installer -O - | bash && mv /root/.symfony5/bin/symfony /usr/local/bin/symfony
CMD [ "pipenv", "run", "python", "./dependency-scan/main.py" ]

###########################
#### Elastic Beanstalk ####
###########################

FROM php74-python-node16 AS php74-eb
RUN git clone https://github.com/aws/aws-elastic-beanstalk-cli-setup.git
RUN python ./aws-elastic-beanstalk-cli-setup/scripts/ebcli_installer.py
ENV PATH="/root/.ebcli-virtual-env/executables:$PATH"
RUN apt-get clean

##############################
#### PHP 7.4 FPM, Node 12 ####
##############################

FROM php:7.4-fpm-buster AS php74-fpm

RUN apt clean
RUN apt update
ADD ./scripts /scripts
ADD ./bin/slack /usr/local/bin
RUN /scripts/php_install.sh
RUN apt install ssl-cert apache2 -yqq
RUN apt-get install jq -yqq
COPY ./apache/default.conf /etc/apache2/sites-available/default.conf
COPY ./apache/php_fpm.conf /etc/apache2/conf-available/php_fpm.conf
RUN sed -i 's|;access.format.*|access.format="%t \"%m %r%{REQUEST_URI}e\""|' /usr/local/etc/php-fpm.d/www.conf
RUN sed -i 's/^pm.max_children = .*/pm.max_children = 20/' /usr/local/etc/php-fpm.d/www.conf
RUN sed -i 's/^pm = dynamic$/pm = static/' /usr/local/etc/php-fpm.d/www.conf
RUN a2ensite default
RUN a2dissite 000-default
RUN a2enmod ssl rewrite actions proxy_fcgi setenvif alias
RUN a2enconf php_fpm

##############################
#### PHP 7.4 FPM, Node 14 ####
##############################

FROM php74-fpm AS php74-node14-fpm
RUN /scripts/node14_install.sh

##############################
#### PHP 7.4 FPM, Node 16 ####
##############################

FROM php74-fpm AS php74-node16-fpm
RUN /scripts/node16_install.sh
