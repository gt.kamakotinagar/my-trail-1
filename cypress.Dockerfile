FROM cypress/browsers:node14.15.0-chrome86-ff82

SHELL ["/bin/bash", "-c"]

RUN yarn global add pm2
RUN yarn global add cypress@7.2.0
RUN yarn global add typescript@3.9.6
RUN yarn global add eslint@7.4.0
RUN yarn global add eslint-config-molindo@5.0.1
RUN cypress install
RUN apt-get install memcached -y
RUN memcached -d -u memcache
